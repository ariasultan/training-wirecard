package com.latihan.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class helloController {

	@RequestMapping(value = "/index")
	public String passParameterWithModelMap(Model model) {
		model.addAttribute("welcomeMessage", "welcome");
		model.addAttribute("message", "Hello Andre");
		return "index";
	}
	
	@GetMapping("/hello")
	public String passParameterWithModel(Model model) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("spring", "mvc");
		model.addAttribute("pesan", "Model");
		model.mergeAttributes(map);
		return "hello";
	}
	
	@GetMapping("/goToViewPage")
	public ModelAndView passParametersWithModelAndView() {
	    ModelAndView modelAndView = new ModelAndView("hello");
	    modelAndView.addObject("message", "lol");
	    return modelAndView;
	}
	
	@GetMapping("/helloworld")
	public ModelAndView update(ModelMap map, Model model) {
		model.addAttribute("hel", "model");
		map.put("hell", "modelmap");
	    ModelAndView modelAndView = new ModelAndView("hello");
	    return modelAndView;
	}
}
