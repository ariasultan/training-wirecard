import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.bhinneka.com/')

WebUI.click(findTestObject('Bhinneka/Page_Bhinneka Toko Online Komputer Elektronik  Gadget Terpercaya/span_Daftar_pt-icon-large pt-icon-log-in'))

WebUI.setText(findTestObject('Object Repository/Bhinneka/Page_Login/input_Account_email'), 'ariasultan4@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Bhinneka/Page_Login/input_Account_password'), 'QiKuTIT0OrlikODdce2amq7cAsyNLESQ')

WebUI.click(findTestObject('Object Repository/Bhinneka/Page_Login/button_Sign In'))

WebUI.click(findTestObject('Object Repository/Bhinneka/Page_Bhinneka Toko Online Komputer Elektronik  Gadget Terpercaya/button_Keluar_toggle-offcanvas-right'))

WebUI.navigateToUrl('https://www.bhinneka.com/member')

result = WebUI.getText(findTestObject('Bhinneka/Page_Dashboard Akun Bhinneka/h3_Lalu Aria Dharma BS SH'))

WebUI.verifyMatch(result, 'Lalu Aria Dharma BS. SH', false)

WebUI.closeBrowser()

