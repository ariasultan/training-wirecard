<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Halo Lalu Aria Dharma</name>
   <tag></tag>
   <elementGuidId>858c3e89-c8ec-4a72-a951-2ac7ca3c24e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;bt-home&quot;)/div[@class=&quot;bt-body-wrapper&quot;]/div[@class=&quot;bt-offcanvas -side-right&quot;]/div[@class=&quot;bt-offcanvas__content&quot;]/div[@class=&quot;bt-offcanvas__title&quot;]/span[@class=&quot;bt-typo-displaysmall&quot;][count(. | //span[(text() = 'Halo, Lalu Aria Dharma' or . = 'Halo, Lalu Aria Dharma')]) = count(//span[(text() = 'Halo, Lalu Aria Dharma' or . = 'Halo, Lalu Aria Dharma')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bt-typo-displaysmall</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Halo, Lalu Aria Dharma</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bt-home&quot;)/div[@class=&quot;bt-body-wrapper&quot;]/div[@class=&quot;bt-offcanvas -side-right&quot;]/div[@class=&quot;bt-offcanvas__content&quot;]/div[@class=&quot;bt-offcanvas__title&quot;]/span[@class=&quot;bt-typo-displaysmall&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='bt-home']/div/div[2]/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akun Saya'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Transaksi'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
   </webElementXpaths>
</WebElementEntity>
