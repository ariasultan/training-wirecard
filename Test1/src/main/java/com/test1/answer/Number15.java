package com.test1.answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Number15 {

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		Scanner scan = new Scanner(System.in);
		try {
			for(int i = 1; i <= 10; i++) {
				System.out.print(i + ". Enter number : ");
				int number = scan.nextInt();
				numbers.add(number);
			}
			double sum = 0;
			for(int i = 0; i < numbers.size(); i++) {
				sum += numbers.get(i);
			}
			double average = sum / numbers.size();
			System.out.println("The numbers above average");
			for(int i = 0; i < numbers.size(); i++) {
				if(numbers.get(i) > average)
					System.out.println(numbers.get(i));
			}
		}
		catch (Exception e) {
			System.out.println("Enter the correct values");
		}
		finally {
			scan.close();
		}
	}

}
