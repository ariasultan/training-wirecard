package com.test1.answer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Number3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			List<Integer> numbers = new ArrayList<Integer>();
			System.out.print("Number 1: ");
			int num1 = scan.nextInt();
			numbers.add(num1);
			System.out.print("Number 2: ");
			int num2 = scan.nextInt();
			numbers.add(num2);
			System.out.print("Number 3: ");
			int num3 = scan.nextInt();
			numbers.add(num3);
			Collections.sort(numbers);
			System.out.println("Sorted numbers: " + numbers.get(0) + " " + numbers.get(1) + " " + numbers.get(2));
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}
}
