package com.test1.answer;

import java.util.Scanner;

public class Number10 {
	
	public static int sumDigits(long n) {
		String numberString = Long.toString(n);
		int sum = 0;
		for(int i = 0; i < numberString.length(); i++) {
			sum += Character.getNumericValue(numberString.charAt(i));
		}
		return sum;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Enter number: ");
			int number = scan.nextInt();
			int sumDigit = sumDigits(number);
			System.out.println("The Sum of each digits: " + sumDigit);
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
