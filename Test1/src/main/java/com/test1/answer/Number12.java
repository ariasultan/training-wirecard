package com.test1.answer;

import java.util.Scanner;

public class Number12 {
	
	public static boolean validateTicket(long ticketNumber) {
		int ticketLength = String.valueOf(ticketNumber).length();
		if(ticketLength >= 10 && ticketLength <= 12) {
			long validator = ticketNumber % 10;
			long theTicket = ticketNumber / 10;
			long remainderOfTheTicket = theTicket % 7;
			if(validator == remainderOfTheTicket)
				return true;
			else
				return false;
		}
		else
			return false;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("Enter ticket number: ");
			long ticket;
			ticket = scan.nextLong();
			while(!validateTicket(ticket)) {
				System.out.print("Enter the correct ticket number: ");
				ticket = scan.nextInt();
			}
			System.out.println("The ticket entered is valid");
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
