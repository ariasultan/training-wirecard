package com.test1.answer;

import java.util.Scanner;

public class Number19 {

	public static void main(String[] args) {
		Double sides[] = new Double[3];
		Scanner scan = new Scanner(System.in);
		try {
			for(int i = 1; i <= 3; i++) {
				System.out.print(i + ". Enter triangle side: ");
				double side = scan.nextDouble();
				sides[i] = side;
			}
			if(MyTriangle.isValid(sides[1], sides[2], sides[3])) {
				double area = MyTriangle.area(sides[1], sides[2], sides[3]);
				System.out.println("The area of the triangle is " + area);
			}
			else
				System.out.println("The triangle is not valid");
		}
		catch (Exception e) {
			System.out.println("Enter the correct values");
		}
		finally {
			scan.close();
		}
	}

}
