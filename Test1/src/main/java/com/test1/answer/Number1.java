package com.test1.answer;

import java.util.Scanner;

public class Number1 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Investment amount: ");
			double investmentAmount = scan.nextDouble();
			System.out.print("Annual investment rate(in %): ");
			double monthlyInvestmentRate = scan.nextDouble() / 12;
			System.out.print("Number of years: ");
			double duration = scan.nextDouble();
			double futureInvestmentValue = investmentAmount * Math.pow((1 + monthlyInvestmentRate / 100), duration * 12);
			System.out.println("Future Investment Value: " + futureInvestmentValue);
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
