package com.test1.answer;

import java.util.Scanner;

public class Number7 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Pattern type: ");
			int patternType = scan.nextInt();
			switch(patternType) {
				case 1:
					for(int i = 1; i <= 6; i++) {
						for(int j = 1; j <= i; j++) {
							System.out.print(j);
						}
						System.out.println();
					}
					break;
				case 2:
					for(int i = 6; i >= 1; i--) {
						for(int j = 1; j <= i; j++) {
							System.out.print(j);
						}
						System.out.println();
					}
					break;
				case 3:
					for(int i = 1; i <= 6; i++) {
						for(int j = 6 - i; j >= 1; j--) {
							System.out.print(" ");
						}
						for(int j = 1; j <= i; j++) {
							System.out.print(j);
						}
						System.out.println();
					}
					break;
				case 4:
					for(int i = 6; i >= 1; i--) {
						for(int j = 6 - i; j >= 1; j--) {
							System.out.print(" ");
						}
						for(int j = 1; j <= i; j++) {
							System.out.print(j);
						}
						System.out.println();
					}
					break;
			}
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
