package com.test1.answer;

import java.util.Scanner;

public class Number13 {
	
	public static double futureInvestmentValue(double investmentValue, double monthlyInterestRate, int years) {
		return investmentValue * Math.pow((1 + monthlyInterestRate / 100), years * 12);
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Investment amount: ");
			double investmentAmount = scan.nextDouble();
			System.out.print("Annual investment rate(in %): ");
			double monthlyInvestmentRate = scan.nextDouble() / 12;
			System.out.println("Years\tFuture Value");
			for(int i = 1; i <= 30; i++) {
				double futureValue = futureInvestmentValue(investmentAmount, monthlyInvestmentRate, i);
				System.out.println(i + "\t" + futureValue);
			}
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
