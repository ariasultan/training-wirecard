package com.test1.answer;

public class Number17 {
	
	public static double average(int[] array) {
		double sum = 0;
		for(int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum * 1.0 / array.length;
	}
	
	public static double average(double[] array) {
		double sum = 0;
		for(int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum / array.length;
	}

	public static void main(String[] args) {
		int intData[] = {1, 2, 3, 4, 5, 6};
		double doubleData[] = {6.0, 4.4, 1.9, 2.9, 3.4, 3.5};
		System.out.println("Average int: " + average(intData));
		System.out.println("Average double: " + average(doubleData));
	}

}
