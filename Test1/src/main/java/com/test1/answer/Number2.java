package com.test1.answer;

import java.util.Scanner;

public class Number2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Enter balance: ");
			double balance = scan.nextDouble();
			System.out.print("Enter annual interest rate: ");
			double annualInterestRate = scan.nextDouble();
			double futureInvestmentValue = balance * (annualInterestRate / 1200);
			System.out.println("The interest is " + futureInvestmentValue);
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}
}
