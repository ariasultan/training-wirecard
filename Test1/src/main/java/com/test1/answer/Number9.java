package com.test1.answer;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Number9 {

	public static void main(String[] args) {
		Random randomGen = new Random();
		List<Integer> numbers = new ArrayList<Integer>();
		int sum = 0;
		System.out.print("The sequence: ");
		for(int i = 0; i < 10; i++) {
			int rand = randomGen.nextInt(1000);
			sum += rand;
			numbers.add(rand);
			System.out.print(rand + " ");
		}
		System.out.println();
		double mean = sum * 1.0 / 9;
		System.out.println("Mean: " + mean);
		double deviate = 0;
		for(int i = 0; i < 10; i++) {
			deviate += Math.pow(numbers.get(i) - mean, 2);
		}
		double deviateMean = deviate / 10;
		double standardDeviation = Math.sqrt(deviateMean);
		System.out.println("Standard Deviation: " + standardDeviation);
	}

}
