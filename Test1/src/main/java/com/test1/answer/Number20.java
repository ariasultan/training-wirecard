package com.test1.answer;

public class Number20 {
	
	public static int sum(int[][] matrix) {
		int sum = 0;
		for(int row = 0; row < matrix.length; row++) {
			for(int col = 0; col < matrix[row].length; col++) {
				sum += matrix[row][col];
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		int[][] matrix = {{1, 2, 4, 5}, {6, 7, 8, 9}, {10, 11, 12, 13}, {14, 15, 16, 17}};
		int sum = sum(matrix);
		System.out.println("The Sum: " + sum);
	}

}
