package com.test1.answer;

import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class Number6 {

	public static void main(String[] args) {
		List<String> studentNames = new ArrayList<String>();
		List<Integer> studentScores = new ArrayList<Integer>();
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("How many students ?:");
			int numberOfStudents = scan.nextInt();
			for(int i = 0; i < numberOfStudents; i++) {
				System.out.println("Student Name: ");
				String name = scan.next();
				System.out.println(name + "'s Score : ");
				int score = scan.nextInt();
				studentNames.add(name);
				studentScores.add(score);
			}
			int highestScore = Collections.max(studentScores);
			String nameHighestScore = studentNames.get(studentScores.indexOf(highestScore));
			System.out.println(nameHighestScore + " get the highest score with " + highestScore);
		}
		catch (Exception e) {
			System.out.println("Enter the correct values");
		}
		finally {
			scan.close();
		}
	}

}
