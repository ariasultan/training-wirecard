package com.test1.answer;

import java.util.Arrays;

public class Number18 {

	public static void main(String[] args) {
		int data[] = {1, 2, 4, 5, 10, 100, 2, -22};
		Arrays.sort(data);
		System.out.println("The smallest number: " + data[0]);
	}

}
