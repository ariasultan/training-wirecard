package com.test1.answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Number16 {

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		Scanner scan = new Scanner(System.in);
		try {
			for(int i = 1; i <= 10; i++) {
				System.out.print(i + ". Enter number : ");
				int number = scan.nextInt();
				numbers.add(number);
			}
			System.out.println("The numbers in reverse order");
			for(int i = numbers.size() - 1; i >= 0; i--) {
				System.out.println(numbers.get(i));
			}
		}
		catch (Exception e) {
			System.out.println("Enter the correct values");
		}
		finally {
			scan.close();
		}
	}

}
