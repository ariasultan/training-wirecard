package com.test1.answer;

import java.util.Scanner;

public class Number14 {
	
	public static double mSumSeries(int n) {
		if (n == 0)
			return 4;
		double sum = 1;
		for(int i = 2; i <= n; i+=2) {
			sum -= 1 / (2.0 * i - 1);
			sum += 1 / (2.0 * i + 1);
		}
		return sum * 4;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Enter I: ");
			int n = scan.nextInt();
			System.out.println("The sum of the m series is " + mSumSeries(n));
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
