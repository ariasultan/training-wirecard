package com.test1.answer;

import java.util.Scanner;

public class Number11 {
	
	public static int reverse(long n) {
		String numberString = Long.toString(n);
		String reverseString = new StringBuilder(numberString).reverse().toString();
		return Integer.parseInt(reverseString);
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Enter number: ");
			int number = scan.nextInt();
			int reverseDigit = reverse(number);
			System.out.println("The Reverse Number: " + reverseDigit);
		}
		catch (Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
