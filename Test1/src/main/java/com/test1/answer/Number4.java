package com.test1.answer;

import java.util.Scanner;

public class Number4 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("Enter an integer number: ");
			int number = scan.nextInt();
			int flag = 0;
			if(number % 5 == 0) flag++;
			if(number % 6 == 0) flag++;
			switch(flag) {
				case 0:
					System.out.println(number + " is not divisible by either 5 or 6");
					break;
				case 1:
					System.out.println(number + " is divisible by 5 or 6, but not both10");
					break;
				case 2:
					System.out.println(number + " is divisible by both 5 or 6");
					break;
			}
		}
		catch(Exception e) {
			System.out.println("Enter the values correctly");
		}
		finally {
			scan.close();
		}
	}

}
