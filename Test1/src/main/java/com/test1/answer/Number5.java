package com.test1.answer;

public class Number5 {

	public static void main(String[] args) {
		System.out.println("Kilograms\tPounds");
		for(int i = 1; i <= 200; i+=2) {
			System.out.println(i + "\t\t" + i * 2.2);
		}
	}
}
