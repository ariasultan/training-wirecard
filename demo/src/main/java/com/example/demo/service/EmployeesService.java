package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Employees;

public interface EmployeesService {

	public Employees findByEmpID(Integer empId);
	public List<Employees> findAll();
	public List<Employees> findEmpByPosID(Integer posId);
	public Map<String, String> setEmp(Employees e);
	public Map<String, String> delEmp(Employees e);
}
