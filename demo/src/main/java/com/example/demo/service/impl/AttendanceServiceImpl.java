package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Attendance;
import com.example.demo.repository.AttendanceRepo;
import com.example.demo.service.AttendanceService;

@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService {

	@Autowired
	AttendanceRepo ar;
	
	@Override
	public Attendance findByAttenId(Integer attenId) {
		Attendance attendance = new Attendance();
		attendance = ar.findByAttenId(attenId);
		return attendance;
	}

	@Override
	public List<Attendance> findAll() {
		List<Attendance> atts = new ArrayList<Attendance>();
		atts = (List<Attendance>) ar.findAll();
		return atts;
	}

	@Override
	public List<Attendance> findAttenByEmpID(Integer empId) {
		List<Attendance> atts = new ArrayList<Attendance>();
		atts = (List<Attendance>) ar.findAttenByEmpID(empId);
		return atts;
	}

	@Override
	public Map<String, String> setAtten(Attendance a) {
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			a = ar.save(a);
			resMap.put("posid", a.getAttenId().toString());
			resMap.put("code", HttpStatus.OK + "");
			resMap.put("msg", "success");
		}
		catch (Exception e2) {
			resMap.put("posid", "");
			resMap.put("code", HttpStatus.CONFLICT + "");
			resMap.put("msg", "failed");
		}
		return resMap;
	}

	@Override
	public Map<String, String> delAtten(Attendance a) {
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			ar.delete(a);
			resMap.put("empid", a.getAttenId().toString());
			resMap.put("code", HttpStatus.OK + "");
			resMap.put("msg", "success");
		}
		catch (Exception e2) {
			resMap.put("empid", "");
			resMap.put("code", HttpStatus.CONFLICT + "");
			resMap.put("msg", "failed");
		}
		return resMap;
	}

}
