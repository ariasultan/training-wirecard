package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Position;
import com.example.demo.repository.PositionRepo;
import com.example.demo.service.PositionService;

@Service
@Transactional
public class PositionServiceImpl implements PositionService {

	@Autowired
	PositionRepo pr;
	
	@Override
	public Position findByPosID(Integer posId) {
		Position position = new Position();
		position = pr.findByPosID(posId);
		return position;
	}

	@Override
	public List<Position> findAll() {
		List<Position> poss = new ArrayList<Position>();
		poss = (List<Position>) pr.findAll();
		return poss;
	}

	@Override
	public Map<String, String> setPos(Position p) {
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			p = pr.save(p);
			resMap.put("posid", p.getPosID().toString());
			resMap.put("code", HttpStatus.OK + "");
			resMap.put("msg", "success");
		}
		catch (Exception ex) {
			resMap.put("posid", "");
			resMap.put("code", HttpStatus.CONFLICT + "");
			resMap.put("msg", "failed");
		}
		return resMap;
	}

	@Override
	public Map<String, String> delPos(Position p) {
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			pr.delete(p);
			resMap.put("empid", p.getPosID().toString());
			resMap.put("code", HttpStatus.OK + "");
			resMap.put("msg", "success");
		}
		catch (Exception ex) {
			resMap.put("empid", "");
			resMap.put("code", HttpStatus.CONFLICT + "");
			resMap.put("msg", "failed");
		}
		return resMap;

	}

}
