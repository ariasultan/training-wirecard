package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Attendance;

public interface AttendanceService {

	public Attendance findByAttenId(Integer attenId);
	public List<Attendance> findAll();
	public List<Attendance> findAttenByEmpID(Integer empId);
	public Map<String, String> setAtten(Attendance a);
	public Map<String, String> delAtten(Attendance a);
}
