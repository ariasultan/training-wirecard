package com.example.demo.service;

import java.util.List;
import java.util.Map;


import com.example.demo.model.Position;

public interface PositionService {

	public Position findByPosID(Integer posId);
	public List<Position> findAll();
	public Map<String, String> setPos(Position p);
	public Map<String, String> delPos(Position p);
}
