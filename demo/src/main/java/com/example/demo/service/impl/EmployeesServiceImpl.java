package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Employees;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.service.EmployeesService;

@Service
@Transactional
public class EmployeesServiceImpl implements EmployeesService {

	@Autowired
	private EmployeesRepo er;
	
	@Override
	public Employees findByEmpID(Integer empId) {
		Employees employee = new Employees();
		employee = er.findByEmpID(empId);
		return employee;
	}

	@Override
	public List<Employees> findAll() {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}

	@Override
	public List<Employees> findEmpByPosID(Integer posId) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findEmpByPosID(posId);
		return empls;
	}

	@Override
	public Map<String, String> setEmp(Employees e) {
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			resMap.put("empid", e.getEmpID().toString());
			resMap.put("code", HttpStatus.OK + "");
			resMap.put("msg", "success");
		}
		catch (Exception e2) {
			resMap.put("empid", "");
			resMap.put("code", HttpStatus.CONFLICT + "");
			resMap.put("msg", "failed");
		}
		return resMap;
	}

	@Override
	public Map<String, String> delEmp(Employees e) {
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			er.delete(e);
			resMap.put("empid", e.getEmpID().toString());
			resMap.put("code", HttpStatus.OK + "");
			resMap.put("msg", "success");
		}
		catch (Exception e2) {
			resMap.put("empid", "");
			resMap.put("code", HttpStatus.CONFLICT + "");
			resMap.put("msg", "failed");
		}
		return resMap;
	}

	
}
