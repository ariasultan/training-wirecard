package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Position;

public interface PositionRepo extends CrudRepository<Position, Integer> {

	public Position findByPosID(Integer posId);
	
}
