package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Attendance;

public interface AttendanceRepo extends CrudRepository<Attendance, Integer> {

	public Attendance findByAttenId(Integer attenId);
	public List<Attendance> findAttenByEmpID(Integer empId);
}
