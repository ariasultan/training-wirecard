package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Attendance;
import com.example.demo.model.Employees;
import com.example.demo.model.Position;
import com.example.demo.service.AttendanceService;
import com.example.demo.service.EmployeesService;
import com.example.demo.service.PositionService;

@RestController
public class MainController {
	
	@Autowired
	private EmployeesService es;
	
	@Autowired
	private PositionService ps;
	
	@Autowired
	private AttendanceService as;
	
	//Employees

	@GetMapping("/getempid")
	public Employees getByEmpID(@RequestParam Integer empId) {
		return es.findByEmpID(empId);
	}
	
	@GetMapping("/getempall")
	public List<Employees> getEmpAll() {
		return es.findAll();
	}
	
	@GetMapping("/getempbyposid")
	public List<Employees> getEmpByPosID(@RequestParam Integer posId) {
		return es.findEmpByPosID(posId);
	}
	
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e) {
		return es.setEmp(e);
	}
	
	@PostMapping("/delemp")
	public Map<String, String> delEmp(@RequestBody Employees e) {
		return es.delEmp(e);
	}
	
	//Position
	
	@GetMapping("/getposid")
	public Position getByPosID(@RequestParam Integer posId) {
		return ps.findByPosID(posId);
	}
	
	@GetMapping("/getposall")
	public List<Position> getPosAll() {
		return ps.findAll();
	}
	
	@PostMapping("/setpos")
	public Map<String, String> setPos(@RequestBody Position p) {
		return ps.setPos(p);
	}
	
	@PostMapping("/delpos")
	public Map<String, String> delPos(@RequestBody Position p) {
		return ps.delPos(p);
	}
	
	//Attendance
	
	@GetMapping("/getattenid")
	public Attendance getByAttenId(@RequestParam Integer attenId) {
		return as.findByAttenId(attenId);
	}
	
	@GetMapping("/getattenall")
	public List<Attendance> getAttenAll() {
		return as.findAll();
	}
	
	@GetMapping("/getattenbyempid")
	public List<Attendance> getEmpByEmpID(@RequestParam Integer empId) {
		return as.findAttenByEmpID(empId);
	}
	
	@PostMapping("/setatten")
	public Map<String, String> setPos(@RequestBody Attendance a) {
		return as.setAtten(a);
	}
	
	@PostMapping("/delatten")
	public Map<String, String> delAtten(@RequestBody Attendance a) {
		return as.setAtten(a);
	}
}
