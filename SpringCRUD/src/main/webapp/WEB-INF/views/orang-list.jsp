<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Orang</title>
</head>
<body>

<a href="addOrang">Tambah Data</a>
<br/>
<table>
	<thead>
		<tr>
		<th>NIK</th>
		<th>Nama Orang</th>
		<th>Action</th>		
		</tr>
	
	<c:choose>
		<c:when test="${empty listOrang}">
		No Data
		</c:when>
			<c:otherwise>
				<c:forEach var="item" items="${listOrang}">
				<tr>
					<td>${item.nik}</td>
					<td>${item.namaOrang}</td>
					<td><a href="orang-edit/${item.nik}">Edit</a> <a href="orang-delete/${item.nik}">Delete</a></td>			
				</tr>
				</c:forEach>
			</c:otherwise>
	</c:choose>
	
	</thead>
</table>
</body>
</html>
