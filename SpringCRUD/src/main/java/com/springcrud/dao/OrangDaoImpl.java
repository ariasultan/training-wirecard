package com.springcrud.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.springcrud.model.Orang;

@Repository
public class OrangDaoImpl extends AbstractDao implements OrangDao {

	@SuppressWarnings("unchecked")
	public List<Orang> getAll() {
		List<Orang> item = null;

		String sql = "From Orang";

		try {
			item = new ArrayList<Orang>();
			getSession().beginTransaction();
			Query query = getSession().createQuery(sql);
			item = query.list();
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			getSession().close();
		}

		return item;
	}

	public Orang getById(String id) {
		Orang item = null;
		try {
			item = new Orang();
			getSession().beginTransaction();
			item = (Orang) getSession().get(Orang.class, id);
		} catch (Exception e) {
			System.err.println("getById barangDaoImpl :" + e);
		} finally {
			getSession().getTransaction().commit();
			getSession().close();
		}

		return item;
	}

	public void delete(Orang entity) {
		try {
			getSession().beginTransaction();
			getSession().delete(entity);
		} catch (Exception e) {
			System.err.println(e);
			getSession().getTransaction().rollback();
		} finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
	}

	public void update(Orang entity) {
		try {
			getSession().beginTransaction();
			getSession().update(entity);
		} catch (Exception e) {
			System.err.println(e);
			getSession().getTransaction().rollback();
		} finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
	}

	public void save(Orang entity) {
		try {
			getSession().beginTransaction();
			getSession().save(entity);
		} catch (Exception e) {
			System.err.println(e);
			getSession().getTransaction().rollback();

		} finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
	}
}
