package com.springcrud.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "orang")
public class Orang {

	@Id
	private String nik;
	
	@Column
	private String namaOrang;
	
	@OneToMany(mappedBy = "orang", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Barang> barang;

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNamaOrang() {
		return namaOrang;
	}

	public void setNamaOrang(String namaOrang) {
		this.namaOrang = namaOrang;
	}
	
	public Set<Barang> getBarang() {
		return barang;
	}

	public void setBarang(Set<Barang> barang) {
		this.barang = barang;
	}
}
