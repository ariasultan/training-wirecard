package com.springcrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "barang")
public class Barang {

	@Id
	private long idBarang;
	
	@Column
	private String namaBarang;
	
	@ManyToOne
	@JoinColumn(name = "nik", referencedColumnName = "nik", insertable = true, updatable = true)
	private Orang orang;
	
	@Transient
	private String nik;

	public long getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(long idBarang) {
		this.idBarang = idBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}

	public Orang getOrang() {
		return orang;
	}

	public void setOrang(Orang orang) {
		this.orang = orang;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}
}
