package com.springcrud.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springcrud.dao.BarangDao;
import com.springcrud.model.Barang;

@Transactional
@Service
public class BarangServiceImpl implements BarangService {

	@Autowired
	BarangDao barangDao;

	public List<Barang> getAll() {

		List<Barang> barangs = null;
		try {
			barangs = new ArrayList<Barang>();
			barangs = barangDao.getAll();

		} catch (Exception e) {
			System.err.println(e);
		}
		return barangs;
	}

	public Barang getById(Long id) {
		Barang barang = null;

		try {
			barang = barangDao.getById(id);
		} catch (Exception e) {
		}
		return barang;
	}

	public void delete(Long id) {
		Barang barang = null;
		try {
			barang = new Barang();
			barang = this.getById(id);
			if (barang != null) {
				barangDao.delete(barang);
			}
		} catch (Exception e) {
			System.err.println(e);
		}

	}

	public void update(Barang entity) {
		try {
			if (entity.getNamaBarang() != null) {
				barangDao.update(entity);
			}
		} catch (Exception e) {
			System.err.println(e);
		}

	}

	public void save(Barang entity) {
		try {
			if (entity.getNamaBarang() != null) {
				System.out.println("MASUK SAVE SERVICE");
				barangDao.save(entity);
				System.out.println("SUDAH SAVE SERVICE");

			}
		} catch (Exception e) {
			System.err.println(e);
		}

	}
}
